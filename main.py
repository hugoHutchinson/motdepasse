#! usr/bin/python
# -*- coding: UTF-8 -*-

# importation des librairies
from random import *
import time
import random
import string
import os
import pyperclip
import shutil
import getpass
import sys


def chiffrement(mdp):
    # déclaration des variables
    lettre_ascii = 0
    result_mdp_chiffrer = ""
    nbRandom = 0
    key = ""

    # nous executerons le code suivant pour chaque caratère du contenu à chiffrer
    for i in range(len(mdp)):
        # On génère un chiffre aléatoire entre 1 et 9
        nbRandom = randint(1, 9)
        # on récupère le code ascii du caractère
        lettre_ascii = ord(mdp[i])
        # on ajoute au code ascii du caratère chiffre aléatoire
        ascii = lettre_ascii + nbRandom
        # on écrit le message en récupérant le caractère en string correspondant au code ascii
        result_mdp_chiffrer = result_mdp_chiffrer + str(chr(ascii))
        # on génère la clé de déchiffrement en y ajoutant le chiffre aléatoire
        key = key + str(nbRandom)
    return result_mdp_chiffrer, key


def dechiffrement(contenu_chiffrer, cle_dechiffrer):
    # déclaration des variables
    nbDechiffre = 0
    message_chiffrer = ""
    message_dechiffrer = ""
    lettre_dechiffrer = ""
    for i in range(len(contenu_chiffrer)):
        nbDechiffre = int(cle_dechiffrer[i])
        lettre_ascii = ord(contenu_chiffrer[i])
        # pour revenir au caratère original nous enlevons au code ascii le chiffre de la clé correspondant au caractère
        ascii = lettre_ascii - nbDechiffre
        message_dechiffrer = message_dechiffrer + str(chr(ascii))
    return message_dechiffrer


def option_navigation():
    bool_success = False
    print("\n--------------------------------------------------")
    print("Que voulez vous faire ?")
    print("\t1 - Trouver un mot de passe")
    print("\t2 - Ajouter un nouveau mot de passe")
    print("\t3 - Génerer un mot de passe aléatoire")
    print("\t4 - Supprimer un mot de passe")
    print("\t5 - Quitter l'application")
    print("--------------------------------------------------")
    choice = input("\nIndiquez le numéro correspondant à l'option désirée : ")
    while bool_success is False:
        try:
            if int(choice) > 6 or int(choice) < 1:
                print("\nL'option n'existe pas ! ")
                choice = input("\nIndiquez le numéro correspondant à l'option désirée : ")
            else:
                bool_success = True
        except:
            print("\nL'option n'existe pas ! ")
            choice = input("\nIndiquez le numéro correspondant à l'option désirée : ")
            bool_success = False
    if int(choice) == 1:
        read_mdp()
    if int(choice) == 2:
        add_mdp()
    if int(choice) == 3:
        create_mdp()
    if int(choice) == 4:
        delete_mdp()
    if int(choice) == 5:
        shutil.copy("./mdp.txt", "./mdp_save.txt")
        print("\nÀ bientôt !")
        sys.exit()


def add_mdp():
    service = input("\nPour quel service voulez vous ajoutez un mot de passe (exemple : youtube): ")
    login = input("\nEntrez votre login: ")
    new_mdp = input("Entrer le mot de passe que vous voulez ajouter: ")
    mdp_chiffre, key = chiffrement(new_mdp)
    file = open("./mdp.txt", "r")
    contenu_fichier = file.read()
    file.close()
    contenu_fichier += "\n{} = {} = {} = {}".format(service, mdp_chiffre, key, login)
    file = open("./mdp.txt", "w")
    file.write(contenu_fichier)
    file.close()


def read_mdp():
    print("\nPour afficher tous les mots de passe, écrivez \"all\".")
    mdps = []
    results = []
    file = open("./mdp.txt", 'r')
    contenu = file.read().split("\n")
    file.close()
    search = input("\nEntrer le nom du service : ")
    if search.lower() == "all":
        for ligne in contenu:
            if ligne != '':
                mdps.append(ligne)
        for mdp in mdps:
            mdp_split = mdp.split(" = ")
            if mdp_split[0] != "App":
                results.append("{} : Login = {} Mot de Passe = {}".format(mdp_split[0], mdp_split[3],
                                                                          dechiffrement(mdp_split[1], mdp_split[2])))
            else:
                results.append("{} : Mot de Passe = {}".format(mdp_split[0], dechiffrement(mdp_split[1], mdp_split[2])))
        if len(results) == 0:
            print("Il y a aucun mot de passe d'enregistré")
        else:
            print("\nLe(s) mot(s) de passe sont les suivants : \n")
            for i, result in enumerate(results):
                print("\t{} - {}".format(i+1, result))
            print("\n")
    else:
        for ligne in contenu:
            if search.lower() in ligne.lower():
                mdps.append(ligne)

        for mdp in mdps:
            mdp_split = mdp.split(" = ")
            if mdp_split[0] == "App":
                results.append("{} : Mot de Passe = {}".format(mdp_split[0], dechiffrement(mdp_split[1], mdp_split[2])))
            else:
                results.append("{} : Login = {} Mot de Passe = {}".format(mdp_split[0], mdp_split[3],
                                                                          dechiffrement(mdp_split[1], mdp_split[2])))
        if len(results) == 0:
            print("Il y a aucun mot de passe correspondant à votre recherche !")
        else:
            print("\nLe(s) mot(s) de passe sont les suivants : \n")
            for i, result in enumerate(results):
                print("\t{} - {}".format(i+1, result))
            print("\n")
    bool_good_choice = False
    choice = input("Voulez-vous copier un mot de passe ? (Y/n) : ")
    while not bool_good_choice:
        if choice.lower() == "y" or choice.lower() == "n":
            if choice.lower() == "y":
                choice_password = input("Indiquez le numéro du mot de passe que vous voulez copier : ")
                mot_de_passe = mdps[int(choice_password) - 1]
                password = dechiffrement(mot_de_passe.split(" = ")[1], mot_de_passe.split(" = ")[2])
                copy_mdp(password)
            bool_good_choice = True
        else:
            print("Indiquer 'Y' pour répondre oui et 'n' pour répondre non !")
            choice = input("Voulez-vous copier un mot de passe ? (Y/n) : ")


def create_mdp():
    number_of_strings = 3
    length_of_string = 8
    caracteres = []
    for x in range(number_of_strings):
        chaine = ''.join(random.choice(string.ascii_letters + string.digits + "!@#$%^&*()") for _ in range(length_of_string))
        caracteres.append(chaine)
        new_mdp = ""
    for i, caractere in enumerate(caracteres):
        if i == 0:
            new_mdp += "{}".format(caractere)
        else:
            new_mdp += "-{}".format(caractere)
    print("Le mot de passe généré est : {}".format(new_mdp))
    bool_good_choice = False
    while not bool_good_choice:
        choice = input("Voulez-vous l'enregistrer ? (Y/n) : ")
        if choice.lower() == "y" or choice.lower() == "n":
            bool_good_choice = True
            if choice.lower() == "y":
                service = input("Pour quel service voulez vous ajoutez ce mot de passe (exemple : youtube) ? : ")
                login = input("Quel est le login ? : ")
                mdp_chiffre, key = chiffrement(new_mdp)
                file = open("./mdp.txt", "r")
                contenu_fichier = file.read()
                file.close()
                contenu_fichier += "\n{} = {} = {} = {}".format(service, mdp_chiffre, key, login)
                file = open("./mdp.txt", "w")
                file.write(contenu_fichier)
                file.close()
            else:
                pass
        else:
            print("Indiquer 'Y' pour répondre oui et 'n' pour répondre non !")

    bool_good_choice = False
    while not bool_good_choice:
        choice = input("Voulez-vous copier le mot de passe ? (Y/n) : ")
        if choice.lower() == "y" or choice.lower() == "n":
            if choice.lower() == "y":
                copy_mdp(new_mdp)
            bool_good_choice = True
        else:
            print("Indiquer 'Y' pour répondre oui et 'n' pour répondre non !")


def copy_mdp(mdp):
    pyperclip.copy(mdp)
    print("Le mot de passe a été ajouté à votre presse-papier. Vous pouvez maintenant le copier n'importe où (ex: Ctrl+V).")


def delete_mdp():
    mdps = []
    results = []
    file = open("./mdp.txt", 'r')
    contenu = file.read().split("\n")
    file.close()
    file = open("./mdp.txt", 'r')
    contenu_to_delete = file.read()
    file.close()
    search = input("\nEntrer le nom du service : ")

    for ligne in contenu:
        if search.lower() in ligne.lower():
            mdps.append(ligne)

    for mdp in mdps:
        mdp_split = mdp.split(" = ")
        if mdp_split[0] == "App":
            print("\nVous ne pouvez pas supprimer le mot de passe de cette application.")
        else:
            results.append("{} : Login = {} Mot de Passe = {}".format(mdp_split[0], mdp_split[3],
                                                                      dechiffrement(mdp_split[1], mdp_split[2])))
    if len(results) == 0:
        print("Il y a aucun mot de passe correspondant à votre recherche !")
    else:
        try:
            print("\nLe(s) mot(s) de passe sont les suivants : \n")
            for i, result in enumerate(results):
                print("\t{} - {}".format(i+1, result))
            print("\n")
            choice_password = input("Indiquez le numéro du mot de passe que vous voulez supprimer : ")
            mot_de_passe = mdps[int(choice_password) - 1]
            contenu_to_delete = contenu_to_delete.replace("\n{}".format(mot_de_passe), "")
            file = open("./mdp.txt", 'w')
            file.write(contenu_to_delete)
            file.close()
            print("\n[+] Le mot de passe a bien été supprimé !")
        except:
            print("\n/!\ Le mot de passe n'a pas été supprimé car il y a eu une erreur !")


def new_connection():
    print("\nC'est votre première connexion !\n")
    mdp_user = input("Indiquer un mot de passe pour l'application : ")
    mdp_user, key = chiffrement(mdp_user)
    login = ""
    contenu_fichier = "App = {} = {} = {}\n".format(mdp_user, key, login)
    try:
        file = open("./mdp.txt", "w")
        file.write(contenu_fichier)
        file.close()
        print("\nCe mot de passe vous servira à accéder à l'application !")
    except:
        print("/!\\ Une erreur est survenue lors de l'écriture du fichier")


def signal_handler():
    pass


if __name__ == '__main__':
    print("----------------------------------------------------")
    print("|  Bienvenue dans le gestionnaire de mot de passe  |")
    print("|             Auteur : Hugo WLOCZYSIAK             |")
    print("----------------------------------------------------")
    try:
        exist_file = os.path.isfile("./mdp.txt")
        if exist_file is True:
            size = os.path.getsize("./mdp.txt")
            if size > 0:
                file_mdp = open("./mdp.txt", "r")
                contenu = file_mdp.read().split("\n")
                file_mdp.close()
                contenu_split = contenu[0].split(" = ")
                mdp_app = contenu_split[1]
                key_mdp = contenu_split[2]
                mdp_app = dechiffrement(mdp_app, key_mdp)
                bool_mdp = False
                count_mdp_wrong = 0
                while bool_mdp is False:
                    # mdp_user = input("\nEnter votre vot de passe : ")
                    mdp_user = getpass.getpass("\nEntrer votre mot de passe : ")
                    if str(mdp_user) == mdp_app:
                        print("\nMot de passe correct !")
                        bool_mdp = True
                    else:
                        count_mdp_wrong += 1
                        print("mauvais mot de passe")
                        if count_mdp_wrong == 3:
                            print("\nAttendez 15 secondes\n")
                            time.sleep(15)
                        if count_mdp_wrong == 5:
                            print("\nAttendez 1 minute\n")
                            time.sleep(60)
                        if count_mdp_wrong == 7:
                            print("\nAttendez 5 minutes\n")
                            time.sleep(300)
                        if count_mdp_wrong == 8:
                            file = open("./mdp.txt", 'w')
                            file.truncate(0)
                            file.close()
                            exit()
            else:
                new_connection()
        else:
            new_connection()

    except:
        print("/!\\ Une erreur est survenue lors de la recherche du fichier contenant les mots de passe")
        time.time(30)
        exit()

    while 1:
        option_navigation()
